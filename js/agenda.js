//inicializo las variables
let arraySeries = [];
let arrayTelefonos = [];
document.getElementById('agregar').disabled=true;

leerSeries();


function nuevaSerie() {
    let serie = document.getElementById("series").value;
    let telefono = document.getElementById("telefonos").value;
    arraySeries.push(serie);
    arrayTelefonos.push(telefono);
    console.log(arraySeries);
    console.log(arrayTelefonos);
    //guardar en localstorage
    localStorage.setItem("series", JSON.stringify(arraySeries))
    document.getElementById("series").value = ""; //limpiar el campo input despues de agregarlo
    localStorage.setItem("telefonos", JSON.stringify(arrayTelefonos))
    document.getElementById("telefonos").value = "";
    leerSeries();
}

function leerSeries() {
    //document.getElementById('agregar').disabled=true;
    if (localStorage.length > 0) {
        let ulSeries = document.getElementById("listaSeries");
        let _arraySeries = JSON.parse(localStorage.getItem("series"));
        let ulTelefonos = document.getElementById("listaTelefono");
        let _arrayTelefonos = JSON.parse(localStorage.getItem("telefonos"));

        borrarSeries();

        let codHTML = "";//uso la opcion 2
        //for(let i=0; i<_arraySeries.lenght;i++); forma completa de escribir el for
        for (let item in _arraySeries) {//posicion uno en este arreglo, recorre hasta el ultimo item;
            // //OPCION 1
            // let li = document.createElement("li");
            // li.innerText = _arraySeries[item];
            // li.className = "list-group-item";
            // li.id = item;
            // li.addEventListener("click", function () {
            //     itemSeleccionado(item);
            // });

            // ulSeries.appendChild(li);


            //OPCION 2
            codHTML = `<li class="list-group-item" id="${item}" 
            onclick="itemSeleccionado(${item})">${_arraySeries[item]}</li>`

            ulSeries.innerHTML = ulSeries.innerHTML + codHTML;

        }

        codHTML1 = "";//uso la opcion 2
        for (let item1 in _arrayTelefonos) {//posicion uno en este arreglo, recorre hasta el ultimo item;

            codHTML1 = `<li class="list-group-item" id="${item1}" 
                    onclick="itemSeleccionado(${item1})">${_arrayTelefonos[item1]}</li>`

            ulTelefonos.innerHTML = ulTelefonos.innerHTML + codHTML1;
        }


        if (arraySeries.length == 0) {
            arraySeries = _arraySeries;
        }
        if (arrayTelefonos.length == 0) {
            arrayTelefonos = _arrayTelefonos;
        }


    } else {
        //localStorage vacio
    }
}

function borrarSeries() {
    let ulSeries = document.getElementById("listaSeries");
    let ulTelefonos = document.getElementById("listaTelefono");


    if (ulSeries.children.length > 0) {
        while (ulSeries.firstChild) {
            ulSeries.removeChild(ulSeries.firstChild);

        }
    }

    if (ulTelefonos.children.length > 0) {
        while (ulTelefonos.firstChild) {
            ulTelefonos.removeChild(ulTelefonos.firstChild);
        }
    }


}

function agregarSerie(event) {
    //console.log(event);
    if (event.keyCode == 13) {
        nuevaSerie();
    }
}

function borrarTodo() {
    localStorage.clear();
    borrarSeries();
    arraySeries = [];
    arrayTelefonos = [];
}

function itemSeleccionado(pid) {
    //reconozco la posicion del item seleccionado
    console.log("item seleccionado" + pid);
    //borro el item del arreglo
    arraySeries.splice(pid, 1);
    arrayTelefonos.splice(pid, 1);
    //actualizar el localstorage
    localStorage.setItem("series", JSON.stringify(arraySeries));
    localStorage.setItem("telefonos", JSON.stringify(arrayTelefonos));
    //Llamo a la funcion que debe dibujar la lista
    leerSeries();
}

function requerido(input) {
    if (input.value != "") {
        //el input tiene texto
        input.className = "form-control is-valid"
        return true;
    } else {
        //el input no tiene texto
        input.className = "form-control is-invalid"
        return false;
    }
}

function revisarNumeros(input) {
    if (input.value != "" && !isNaN(input.value)) {
        console.log("esta bien")
        input.className = "form-control is-valid";
        return true;
    } else {
        input.className = "form-control is-invalid";
        return false;
    }
}

function validarGeneral(eventoo) {
    event.preventDefault();
    console.log("hola")
  
    
    if (requerido(document.getElementById("series")) &&
        requerido(document.getElementById("tel")))//&&
        //revisarNumeros(document.getElementById("tel")))
        {
            console.log("entra");
        alert("El formulario esta listo para ser enviado");
        document.getElementById('agregar').disabled=false;
    } else {
        alert("Ocurrio un error");
    }
}